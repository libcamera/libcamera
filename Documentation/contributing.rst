Contributing
============

libcamera is developed as a free software project and welcomes contributors.
Whether you would like to help with coding, documentation, testing, proposing
new features, or just discussing the project with the community, you can join
our official public communication channels, or simply check out the code.

Mailing List
------------

We use a public mailing list as our main means of communication. You can find
subscription information and the messages archive on the `libcamera-devel`_
list information page.

.. _libcamera-devel: https://lists.libcamera.org/listinfo/libcamera-devel

IRC Channel
-----------

For informal and real time discussions, our IRC channel on Freenode is open to
the public. Point your IRC client to #libcamera to say hello, or use the `WebChat`_.

.. _WebChat: https://webchat.freenode.net/?channels=%23libcamera&uio=d4

Source Code
-----------

libcamera is in early stages of development, and no releases are available yet.
The source code is available from the project's git tree, hosted by LinuxTV.

  $ git clone git://linuxtv.org/libcamera.git

Documentation
-------------

Project documentation is created using `Sphinx`_.  Source level documentation
is currently planned to utilise Doxygen integration.  Please use this in your
development.

Sphinx integration with Doxygen will be with either `Breathe`_ or `Exhale`_
depending upon which system works best

.. _Sphinx: http://www.sphinx-doc.org
.. _Breathe: https://breathe.readthedocs.io/en/latest/
.. _Exhale: https://exhale.readthedocs.io/en/latest/
